package Util;

/**
 *
 * @author Codigo Patito
 */
public enum Type {

    TEXT, NUMBER, NUMBER_INTEGERS, EMAIL, TEXT_LETTERS, PASSWORD;

}
