package Components.Controls;

import Util.Attributes;
import Util.Colors;
import Util.RoundedBorder;
import Util.Type;
import Util.Validations;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author Codigo Patito
 */
public class InputPassword extends Attributes {

    private final int radius = 12;
    private final Validations validations = new Validations();
    private String value = "";
    private boolean showButtonViewPassword;
    private final JLabel lblIcon;
    private boolean showPassword;
    private Validations util = new Validations();

    public InputPassword() {
        setOpaque(false);
        setFont(new Font("Tahoma", 0, 14));
        setBorder(new RoundedBorder(5, Colors.TRANSPARENT));

        lblIcon = new JLabel();
        lblIcon.setCursor(new Cursor(Cursor.HAND_CURSOR));
        lblIcon.setSize(30, 25);
        lblIcon.setIcon(util.resizeIcon("/Image/eye.png", lblIcon.getWidth(), lblIcon.getHeight()));
        lblIcon.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (isShowButtonViewPassword()) {
                    showPassword = !showPassword;
                    setText(value);
                    lblIcon.setIcon(util.resizeIcon("/Image/eye" + (showPassword ? "2" : "") + ".png", lblIcon.getWidth(), lblIcon.getHeight()));
                }
            }
        });
        lblIcon.setVisible(false);
        this.add(lblIcon);
        revalidate();

        this.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyChar() > 31 && e.getKeyChar() < 127) {
                    value += e.getKeyChar();
                    setText(value);
                }
                if (e.getExtendedKeyCode() == 8) {
                    if (value.length() > 0) {
                        value = value.substring(0, value.length() - 1);
                        setText(value);
                    }
                }
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getExtendedKeyCode() == 8) {
                    if (value.length() > 0) {
                        value = value.substring(0, value.length() - 1);
                        setText(value);
                    }
                }
            }
        });
    }

    @Override
    public void setType(Type type) {
        super.setType(Type.PASSWORD);
    }

    @Override
    public void setText(String t) {
        value = t;
        if (!showPassword) {
            t = "";
            for (int i = 0; i < value.length(); i++) {
                t += "*";
            }
        }
        super.setText(t);
    }

    @Override
    public String getText() {
        return value;
    }

    public String getTextMD5() {
        return getHash("MD5");
    }

    public String getTextSHA512() {
        return getHash("SHA-512");
    }

    private String getHash(String type) {
        try {
            MessageDigest md = MessageDigest.getInstance(type);
            byte[] digest = md.digest(value.getBytes("UTF-8"));
            return bytesToHex(digest);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(InputPassword.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public boolean isShowButtonViewPassword() {
        return showButtonViewPassword;
    }

    public void setShowButtonViewPassword(boolean showButtonViewPassword) {
        lblIcon.setLocation(getWidth() - 40, (getHeight() / 2) - 15);
        lblIcon.setVisible(showButtonViewPassword);
        this.showButtonViewPassword = showButtonViewPassword;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Colors.WHITE);
        if (!isEnabled() || !isEditable()) {
            g2.setColor(new Color(getBackground().getRed() / 255F, getBackground().getGreen() / 255F, getBackground().getBlue() / 255F, 0.5F));
        }
        g2.fillRoundRect(1, 1, getWidth() - 1, getHeight() - 1, radius, radius);

        if (validations.isNullOrEmpty(getText()) && !validations.isNullOrEmpty(getPlaceHolder())) {
            g2.setColor(getDisabledTextColor());
            g2.setFont(getFont());
            g2.drawString(getPlaceHolder(), getInsets().left, (getHeight() / 2) + 5);
        }
        super.paintComponent(g2);
    }

    @Override
    protected void paintBorder(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        validations.applyQualityRenderingHints(g2);
        if (validate) {
            g2.setStroke(new BasicStroke(2));
            boolean val = isValidation();
            g2.setColor(Colors.getColorOpacity(val ? Colors.SUCCESS : Colors.DANGER, 0.6F));
            if (!validations.isNullOrEmpty(getValidFeedback()) || !validations.isNullOrEmpty(getInvalidFeedback())) {
                g2.setFont(new Font("Tahoma", 0, 12));
                if (val && !validations.isNullOrEmpty(getValidFeedback())) {
                    g2.drawString(getValidFeedback(), 3, getHeight() - 4);
                }
                if (!val && !validations.isNullOrEmpty(getInvalidFeedback())) {
                    g2.drawString(getInvalidFeedback(), 3, getHeight() - 4);
                }
            }
        } else {
            if (isFocusOwner() && isEnabled() && isEditable()) {
                g2.setColor(Colors.getColorOpacity(Color.decode("#86b7fe"), 0.5F));
                g2.setStroke(new BasicStroke(2));
            } else {
                g2.setColor(Colors.getColorOpacity(Colors.SECONDARY, 0.7F));
                g2.setStroke(new BasicStroke(1));
            }
        }
        g2.drawRoundRect(0, 0, getWidth(), getHeight(), radius, radius);
    }

}
