package Components.Buttons;

import Util.Colors;
import Util.Roboto;
import Util.TypeButton;
import Util.Validations;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.ImageObserver;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author Codigo Patito
 */
public class BootstrapButtonOutLine extends JButton {

    private TypeButton type = TypeButton.PRIMARY;
    private Color color;
    private Color textColor;
    private Color colorHover;
    private Image image;
    private ImageObserver imageObserver;
    private ImageIcon icon;
    private final Validations validations = new Validations();

    public BootstrapButtonOutLine() {
        setPreferredSize(new Dimension(200, 40));
        setSize(new Dimension(200, 40));
        setFont(Roboto.BOLD.deriveFont(14.0F));
        setOpaque(false);
        setContentAreaFilled(false);
        setFocusPainted(false);
        setBorderPainted(false);
    }

    public TypeButton getType() {
        return this.type;
    }

    public void setType(TypeButton type) {
        this.type = type;
    }

    public Color getColor() {
        if (this.color != null) {
            return this.color;
        }
        return TypeButton.getColor(getType());
    }

    public void setColor(String color) {
        this.color = Color.decode(color);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getTextColor() {
        if (this.textColor != null) {
            return this.textColor;
        }
        return TypeButton.getTextColor(getType());
    }

    public void setTextColor(String textColor) {
        this.textColor = Color.decode(textColor);
    }

    public Color getColorHover() {
        if (this.colorHover != null) {
            return this.colorHover;
        }
        return TypeButton.getColorHover(getType());
    }

    public void setColorHover(String colorHover) {
        this.colorHover = Color.decode(colorHover);
    }

    public void setColorHover(Color colorHover) {
        this.colorHover = colorHover;
    }

    @Override
    public ImageIcon getIcon() {
        return icon;
    }

    public void setIcon(ImageIcon defaultIcon) {
        image = defaultIcon.getImage();
        imageObserver = defaultIcon.getImageObserver();
        this.repaint();
        super.setIcon(defaultIcon); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        validations.applyQualityRenderingHints(g2);

        g2.translate(5, 5);
        g2.setStroke(new BasicStroke(4f));
        ButtonModel m = getModel();

        setForeground(Colors.TRANSPARENT);
        if (m.isRollover()) {
            setForeground(getColorHover());
        }
        RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0F, 0F, (getWidth() - 10), (getHeight() - 10), 15F, 15F);

        if (m.isArmed()) {
            r2d = new RoundRectangle2D.Float(2F, 2F, (getWidth() - 15), (getHeight() - 15), 15F, 15F);
        }

        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        if (!m.isEnabled()) {
            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            setForeground(new Color(getColor().getRed() / 255F, getColor().getGreen() / 255F, getColor().getBlue() / 255F, 0.1F));
        }

        g2.fill(r2d);

        g2.setColor(getColor());

        g2.setStroke(new BasicStroke(1.2F));
        g2.draw(r2d);

        g2.setFont(getFont());
        g2.setColor(getColor());

        if (m.isRollover()) {
            g2.setColor(getTextColor());
        }

        FontMetrics fm = g2.getFontMetrics();
        int x = (getWidth() - 10 - fm.stringWidth(getText())) / 2;
        int y = (fm.getAscent() + (getHeight() - 10 - (fm.getAscent() + fm.getDescent())) / 2);
        g2.drawString(getText(), x, y);

        if (image != null) {
            if (m.isArmed()) {
                g2.drawImage(image, 2, 2, getWidth() - 15, getHeight() - 15, imageObserver);
            } else {
                g2.drawImage(image, 0, 0, getWidth() - 10, getHeight() - 10, imageObserver);
            }
        }
    }

}
