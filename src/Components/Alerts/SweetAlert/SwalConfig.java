package Components.Alerts.SweetAlert;

/**
 *
 * @author Codigo Patito
 */
public class SwalConfig {

    private String status;
    private String title;
    private String text;
    private String textButtonCancel;
    private String colorButtonCancel;
    private String textButtonOk;
    private String colorButtonOk;
    private int durationClose;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextButtonCancel() {
        return textButtonCancel;
    }

    public void setTextButtonCancel(String textButtonCancel) {
        this.textButtonCancel = textButtonCancel;
    }

    public String getColorButtonCancel() {
        return colorButtonCancel;
    }

    public void setColorButtonCancel(String colorButtonCancel) {
        this.colorButtonCancel = colorButtonCancel;
    }

    public String getTextButtonOk() {
        return textButtonOk;
    }

    public void setTextButtonOk(String textButtonOk) {
        this.textButtonOk = textButtonOk;
    }

    public String getColorButtonOk() {
        return colorButtonOk;
    }

    public void setColorButtonOk(String colorButtonOk) {
        this.colorButtonOk = colorButtonOk;
    }

    public int getDurationClose() {
        if (durationClose < 1) {
            durationClose = 800;
        }
        return durationClose;
    }

    public void setDurationClose(int durationClose) {
        this.durationClose = durationClose;
    }

}
