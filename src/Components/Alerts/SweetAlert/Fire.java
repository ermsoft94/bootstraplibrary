package Components.Alerts.SweetAlert;

import Util.Colors;
import Util.RoundedBorder;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

/**
 *
 * @author Codigo Patito
 */
public final class Fire extends Swal {

    private final SwalConfig config;

    /**
     * Creates new form Fire
     *
     * @param parent
     * @param modal
     * @param config
     */
    public Fire(java.awt.Frame parent, boolean modal, SwalConfig config) {
        super(parent, modal);
        initComponents();
        this.config = config;
        load(parent, config);
    }

    private void load(Frame parent, SwalConfig config) {
        ImageIcon img = new ImageIcon(getClass().getResource("/Image/" + config.getStatus() + ".png"));
        Image imgEscalada = img.getImage().getScaledInstance(lblImage.getWidth(), lblImage.getHeight(), Image.SCALE_SMOOTH);
        Icon iconoEscalado = new ImageIcon(imgEscalada);
        lblImage.setIcon(iconoEscalado);
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitle.setFont(new java.awt.Font("Tahoma", 1, 18));
        lblTitle.setText(config.getTitle());

        lblText.setHorizontalAlignment(SwingConstants.CENTER);
        lblText.setFont(new java.awt.Font("Tahoma", 1, 15));
        lblText.setText(config.getText());

        Dimension d = new Dimension(508, 326);
        panelBody.setPreferredSize(d);
        panelBody.setBackground(Colors.TRANSPARENT);
        panelBody.setBorder(new RoundedBorder(17));

        SpringLayout sl = new SpringLayout();
        sl.putConstraint(SpringLayout.HORIZONTAL_CENTER, panelBody, 0, SpringLayout.HORIZONTAL_CENTER, this);
        sl.putConstraint(SpringLayout.VERTICAL_CENTER, panelBody, 0, SpringLayout.VERTICAL_CENTER, this);
        setLayout(sl);

        setPreferredSize(new Dimension(parent.getWidth() - 16, parent.getHeight() - 16));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelBody = new javax.swing.JPanel();
        lblImage = new javax.swing.JLabel();
        bootstrapButton1 = new Components.Buttons.BootstrapButton();
        lblTitle = new javax.swing.JLabel();
        lblText = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        bootstrapButton1.setText("Ok");
        bootstrapButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bootstrapButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBodyLayout = new javax.swing.GroupLayout(panelBody);
        panelBody.setLayout(panelBodyLayout);
        panelBodyLayout.setHorizontalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBodyLayout.createSequentialGroup()
                .addGroup(panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelBodyLayout.createSequentialGroup()
                        .addGap(205, 205, 205)
                        .addComponent(bootstrapButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(panelBodyLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
            .addGroup(panelBodyLayout.createSequentialGroup()
                .addGap(184, 184, 184)
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(208, Short.MAX_VALUE))
        );
        panelBodyLayout.setVerticalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBodyLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblText, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bootstrapButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelBody, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelBody, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void bootstrapButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bootstrapButton1ActionPerformed
        closeAlert(config.getDurationClose());
    }//GEN-LAST:event_bootstrapButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private Components.Buttons.BootstrapButton bootstrapButton1;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel lblText;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JPanel panelBody;
    // End of variables declaration//GEN-END:variables
}
