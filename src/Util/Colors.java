package Util;

import java.awt.Color;

/**
 *
 * @author Codigo Patito
 */
public class Colors {

    public static final Color PRIMARY = Color.decode("#0d6efd");
    public static final Color PRIMARY_HOVER = Color.decode("#0b5ed7");

    public static final Color SECONDARY = Color.decode("#6c757d");
    public static final Color SECONDARY_HOVER = Color.decode("#5c636a");

    public static final Color SUCCESS = Color.decode("#198754");
    public static final Color SUCCESS_HOVER = Color.decode("#157347");

    public static final Color DANGER = Color.decode("#dc3545");
    public static final Color DANGER_HOVER = Color.decode("#bb2d3b");

    public static final Color WARNING = Color.decode("#ffc107");
    public static final Color WARNING_HOVER = Color.decode("#ffca2c");

    public static final Color INFO = Color.decode("#0dcaf0");
    public static final Color INFO_HOVER = Color.decode("#31d2f2");

    public static final Color LIGHT = Color.decode("#f8f9fa");
    public static final Color LIGHT_HOVER = Color.decode("#d3d4d5");

    public static final Color DARK = Color.decode("#343a40");
    public static final Color DARK_HOVER = Color.decode("#23272b");

    public static final Color WHITE = Color.decode("#ffffff");
    public static final Color BLACK = Color.decode("#000000");
    public static final Color TRANSPARENT = new Color(0, 0, 0, 0);

    public static Color getColorOpacity(Color color, float opacity) {
        return new Color(color.getRed() / 255F, color.getGreen() / 255F, color.getBlue() / 255F, opacity);
    }

}
