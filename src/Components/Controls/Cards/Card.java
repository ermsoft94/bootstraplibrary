package Components.Controls.Cards;

import Util.Colors;
import Util.Validations;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author Codigo Patito
 */
public class Card extends JPanel {

    private boolean hover;
    private boolean mouseHover;
    private int pixels;
    private boolean dark;
    private String title;
    private String text;
    private final JLabel lblTitle = new JLabel("Card Title");
    private final JLabel lblText = new JLabel("");
    private Font fontTitle;
    private Font fontText;
    private Image image;
    private ImageObserver imageObserver;
    private final Validations validations = new Validations();

    protected int strokeSize = 1;
    protected Dimension arcs = new Dimension(12, 12);

    public boolean isHover() {
        return hover;
    }

    public void setHover(boolean hover) {
        this.hover = hover;
        repaint();
    }

    private boolean isMouseHover() {
        return mouseHover;
    }

    private void setMouseHover(boolean mouseHover) {
        this.mouseHover = mouseHover;
        repaint();
    }

    public boolean isDark() {
        return dark;
    }

    public void setDark(boolean dark) {
        this.dark = dark;
    }

    public void setImage(ImageIcon image) {
        this.image = image.getImage();
        imageObserver = image.getImageObserver();
        revalidate();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        repaint();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        repaint();
    }

    public Font getFontText() {
        return fontText;
    }

    public void setFontText(Font fontText) {
        this.fontText = fontText;
    }

    public Font getFontTitle() {
        return fontTitle;
    }

    public void setFontTitle(Font fontTitle) {
        this.fontTitle = fontTitle;
    }

    private void updateLabelTitle() {
        remove(lblTitle);
        lblTitle.setText(getTitle());
        lblTitle.setFont(getFontTitle() != null ? getFontTitle() : new java.awt.Font("Tahoma", 1, 13));
        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle.setBounds(5, (getWidth() / 4) + (getWidth() / 3), getWidth() - 16, 30);
        add(lblTitle);
        revalidate();
    }

    private void updateLabelText() {
        remove(lblText);
        lblText.setText("<html><p style='text-align: justify;'>" + getText().replaceAll("\\n", "<br />") + "</p></html>");
        lblText.setFont(getFontText() != null ? getFontText() : new java.awt.Font("Tahoma", 0, 11));
        lblText.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblText.setBounds(10, (getWidth() / 4) + (getWidth() / 3) + 27, getWidth() - 25, (getHeight() / 4) + (getHeight() / 3) - 30);
        add(lblText);
        revalidate();
    }

    private Color getBackgroundCard() {
        return String.format("#%06X", (0xFFFFFF & this.getBackground().getRGB())).equalsIgnoreCase("#EEEEEE") ? Colors.WHITE : this.getBackground();
    }

    public void setBackground(String color) {
        super.setBackground(Color.decode(color));
    }

    public Card() {
        this.pixels = 5;
        setSize(new Dimension(197, 276));
        setOpaque(false);
        Border border = BorderFactory.createEmptyBorder(pixels, pixels, pixels, pixels);
        this.setBorder(BorderFactory.createCompoundBorder(this.getBorder(), border));
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                if (isHover()) {
                    pixels = 8;
                    setMouseHover(true);
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (isHover()) {
                    pixels = 5;
                    setMouseHover(false);
                }
            }

        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        validations.applyQualityRenderingHints(g2);
        int shade = 0;
        int topOpacity = 80;
        int radius = (pixels % 2 == 0 ? pixels / 2 : (pixels / 2) + 1);
        for (int i = 0; i < pixels; i++) {
            g2.setColor(new Color(shade, shade, shade, ((topOpacity / pixels) * i)));
            if (isMouseHover()) {
                g2.drawLine(width - i, height - radius - 4 - i, width - i, ((radius) - i <= 0 ? radius : (pixels) - i));//linea vertical
                g2.drawLine(((radius) - i <= 0 ? radius : (pixels) - i), height - i, width - pixels - i, height - i);//linea horizontal
                g2.drawArc(width - (pixels + 6) - i, height - (pixels + 6) - i, 14, 14, -90, 90);//arco inferior derecho
            } else {
                g2.drawLine(width - 3 - i, height - radius - 8 - i, width - 3 - i, ((radius) - i <= 0 ? radius : (pixels) - i));//linea vertical
                g2.drawLine(((radius) - i <= 0 ? radius : (pixels) - i), height - 3 - i, width - 4 - pixels - i, height - 3 - i);//linea horizontal
                g2.drawArc(width - (pixels + 10) - i, height - (pixels + 10) - i, 12, 10, -95, 95);//arco inferior derecho
            }
        }
        g2.setColor(Color.decode("#607D8B"));
        g2.setStroke(new BasicStroke(strokeSize));
        g2.drawRoundRect(0, 0, width - (8), height - (8), arcs.width, arcs.height);
        g2.setColor(isDark() ? Colors.getColorOpacity(Color.decode("#263238"), 0.6F) : getBackgroundCard());
        g2.fillRoundRect(1, 1, width - (8 + 1), height - (8 + 1), arcs.width, arcs.height);
        if (image != null) {
            g2.drawImage(image, 1, 1, width - 9, (getWidth() / 4) + (getWidth() / 3), imageObserver);
        }
        if (!validations.isNullOrEmpty(getTitle())) {
            updateLabelTitle();
        }
        if (!validations.isNullOrEmpty(getText())) {
            updateLabelText();
        }
    }

}
