package Components.Alerts.SweetAlert;

import Util.Colors;
import Util.RoundedBorder;
import Util.TypeButton;
import Util.Validations;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

/**
 *
 * @author Codigo Patito
 */
public final class FireConfirm extends Swal {

    private final Validations validations = new Validations();
    private final SwalConfig config;

    /**
     * Creates new form Fire
     *
     * @param parent
     * @param modal
     * @param config
     */
    public FireConfirm(java.awt.Frame parent, boolean modal, SwalConfig config) {
        super(parent, modal);
        initComponents();
        this.config = config;
        load(parent, config);
    }

    private void load(Frame parent, SwalConfig config) {
        ImageIcon img = new ImageIcon(getClass().getResource("/Image/" + config.getStatus() + ".png"));
        Image imgEscalada = img.getImage().getScaledInstance(lblImage.getWidth(), lblImage.getHeight(), Image.SCALE_SMOOTH);
        Icon iconoEscalado = new ImageIcon(imgEscalada);
        lblImage.setIcon(iconoEscalado);

        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitle.setFont(new java.awt.Font("Tahoma", 1, 18));
        lblTitle.setText(config.getTitle());

        lblText.setHorizontalAlignment(SwingConstants.CENTER);
        lblText.setFont(new java.awt.Font("Tahoma", 1, 15));
        lblText.setText(config.getText());

        Dimension d = new Dimension(508, 326);
        panelBody.setPreferredSize(d);
        panelBody.setBackground(Colors.TRANSPARENT);
        panelBody.setBorder(new RoundedBorder(17));

        SpringLayout sl = new SpringLayout();
        sl.putConstraint(SpringLayout.HORIZONTAL_CENTER, panelBody, 0, SpringLayout.HORIZONTAL_CENTER, this);
        sl.putConstraint(SpringLayout.VERTICAL_CENTER, panelBody, 0, SpringLayout.VERTICAL_CENTER, this);
        setLayout(sl);

        setPreferredSize(new Dimension(parent.getWidth() - 16, parent.getHeight() - 16));
        btnCancel.setType(TypeButton.DANGER);

        btnOk.setText(validations.isNullOrEmpty(config.getTextButtonOk()) ? "Ok" : config.getTextButtonOk());
        btnCancel.setText(validations.isNullOrEmpty(config.getTextButtonCancel()) ? "Ok" : config.getTextButtonCancel());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelBody = new javax.swing.JPanel();
        lblImage = new javax.swing.JLabel();
        btnOk = new Components.Buttons.BootstrapButton();
        lblTitle = new javax.swing.JLabel();
        lblText = new javax.swing.JLabel();
        btnCancel = new Components.Buttons.BootstrapButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnOk.setText("Ok");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancelar");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBodyLayout = new javax.swing.GroupLayout(panelBody);
        panelBody.setLayout(panelBodyLayout);
        panelBodyLayout.setHorizontalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBodyLayout.createSequentialGroup()
                .addGap(184, 184, 184)
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(208, Short.MAX_VALUE))
            .addGroup(panelBodyLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBodyLayout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelBodyLayout.setVerticalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBodyLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblText, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelBody, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelBody, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        closeAlert(true, config.getDurationClose());
    }//GEN-LAST:event_btnOkActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        closeAlert(false, config.getDurationClose());
    }//GEN-LAST:event_btnCancelActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private Components.Buttons.BootstrapButton btnCancel;
    private Components.Buttons.BootstrapButton btnOk;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel lblText;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JPanel panelBody;
    // End of variables declaration//GEN-END:variables
}
