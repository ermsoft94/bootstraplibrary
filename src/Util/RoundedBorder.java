package Util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.Border;

/**
 *
 * @author Codigo Patito
 */
public class RoundedBorder implements Border {

    private final int radius;
    private final Color color;

    public RoundedBorder(int radius) {
        this.radius = radius;
        this.color = Colors.WHITE;
    }

    public RoundedBorder(int radius, Color color) {
        this.radius = radius;
        this.color = color;
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return new Insets(this.radius + 1, this.radius + 1, this.radius + 2, this.radius);
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        g.setColor(color);
//        g.drawRoundRect(x, y, width - 1, height - 1, radius, radius);
        g.fillRoundRect(x, y, width - 1, height - 1, radius, radius);
    }

}
