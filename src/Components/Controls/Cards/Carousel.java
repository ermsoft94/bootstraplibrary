package Components.Controls.Cards;

import Util.Colors;
import Util.Validations;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
//import javax.swing.JLabel;
import javax.swing.JPanel;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.TimingTargetAdapter;

/**
 *
 * @author Codigo Patito
 */
public final class Carousel extends JPanel {

//    private String[] title;
//    private String[] text;
//    private final JLabel lblTitle = new JLabel("Title");
//    private final JLabel lblText = new JLabel("");
    private Font fontTitle;
    private Font fontText;
    private String[] images = new String[]{"/Image/slide1.png", "/Image/slide2.png", "/Image/slide3.png"};
    private Animator animator;
    private final JButton btnBack = new JButton();
    private final JButton btnNext = new JButton();
    private final Validations validations = new Validations();
    private boolean controls;
    private boolean autoplaying;

    protected int positionImage;
    protected boolean direction;
    protected String imageCurrent;
    protected String imagePrevious;
    protected String imageNext;
    protected int x;
    protected Thread hilo;
    protected boolean isFirst = true;

    public Carousel() {
        setOpaque(false);
        setLayout(null);
        setSize(new Dimension(276, 197));
        setBorder(BorderFactory.createLineBorder(Color.black));

        imageCurrent = getImages()[0];
        imagePrevious = getImages()[0];
        imageNext = getImages()[0];

        btnBack.setIcon(validations.resizeIcon("/Image/back.png", 25, 25));
        btnBack.setRolloverIcon(validations.resizeIcon("/Image/back_rollover.png", 25, 25));
        btnBack.setContentAreaFilled(false);
        btnBack.setBorderPainted(false);
        btnBack.setFocusPainted(false);
        btnBack.setOpaque(false);
        btnBack.setBackground(Colors.TRANSPARENT);
        btnBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBack.setVisible(false);

        btnNext.setIcon(validations.resizeIcon("/Image/next.png", 25, 25));
        btnNext.setRolloverIcon(validations.resizeIcon("/Image/next_rollover.png", 25, 25));
        btnNext.setContentAreaFilled(false);
        btnNext.setBorderPainted(false);
        btnNext.setFocusPainted(false);
        btnNext.setOpaque(false);
        btnNext.setBackground(Colors.TRANSPARENT);
        btnNext.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNext.setVisible(false);

        btnBack.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                animationCarousel(false);
            }

        });

        btnNext.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                animationCarousel(true);
            }

        });

        add(btnBack);
        add(btnNext);
//        add(lblTitle);
//        add(lblText);
        setAutoplaying(true);
        autoPlaying();
    }

    private void autoPlaying() {
        Runnable runnable = () -> {
            while (isAutoplaying()) {
                try {
                    Thread.sleep(6000);
                    if (isAutoplaying()) {
                        animationCarousel(true);
                    }
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
            }
        };
        hilo = new Thread(runnable);
        hilo.start();
    }

    private void animationCarousel(boolean next) {
        if ((animator == null) || (!animator.isRunning())) {
            animator = new Animator(800, new TimingTargetAdapter() {

                @Override
                public void begin() {
                    getPositionNextBackImage(next);
                }

                @Override
                public void timingEvent(float fraction) {
                    x = (int) (getWidth() * fraction);
                    repaint();
                }

            });
            animator.setResolution(0);
            animator.start();
        } else {
            animator.stop();
        }
    }

//    public String[] getTitle() {
//        return title;
//    }
//
//    public void setTitle(String[] title) {
//        this.title = title;
//        updateLabelTitle();
//    }
//
//    public String[] getText() {
//        return text;
//    }
//
//    public void setText(String[] text) {
//        this.text = text;
//        updateLabelText();
//    }
//
//    private void updateLabelTitle() {
//        lblTitle.setText(getTitle()[positionImage]);
//        lblTitle.setFont(getFontTitle() != null ? getFontTitle() : new java.awt.Font("Tahoma", 1, 13));
//        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
//        lblTitle.setForeground(Color.white);
//        lblTitle.setBounds(2, getHeight() - (getHeight() / 3), getWidth() - 5, 25);
//        revalidate();
//    }
//
//    private void updateLabelText() {
//        lblText.setText("<html><p style='text-align: justify;'>" + getText()[positionImage].replaceAll("\\n", "<br />") + "</p></html>");
//        lblText.setFont(getFontText() != null ? getFontText() : new java.awt.Font("Tahoma", 0, 11));
//        lblText.setVerticalAlignment(javax.swing.SwingConstants.TOP);
//        lblText.setForeground(Color.white);
//        lblText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
//        lblText.setBounds(2, getHeight() - (getHeight() / 3) + 25, getWidth() - 5, (getHeight() / 3) - 40);
//        revalidate();
//    }
    private Image getImage(String path) {
        if (isFirst) {
            return new ImageIcon(getClass().getResource(path)).getImage();
        }
        return new ImageIcon(path).getImage();
    }

    public Font getFontTitle() {
        return fontTitle;
    }

    public void setFontTitle(Font fontTitle) {
        this.fontTitle = fontTitle;
    }

    public Font getFontText() {
        return fontText;
    }

    public void setFontText(Font fontText) {
        this.fontText = fontText;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
        isFirst = false;
        imageCurrent = getImages()[0];
        imagePrevious = getImages()[0];
        imageNext = getImages()[0];
        this.repaint();
    }

    public boolean isControls() {
        return controls;
    }

    public void setControls(boolean controls) {
        this.controls = controls;
        addButtonsControl();
    }

    public boolean isAutoplaying() {
        return autoplaying;
    }

    public void setAutoplaying(boolean autoplaying) {
        this.autoplaying = autoplaying;
        if (isAutoplaying()) {
            if (hilo != null) {
                hilo.resume();
            }
        } else {
            if (hilo != null) {
                hilo.suspend();
            }
        }
    }

    private void addButtonsControl() {
        if (isControls()) {
            btnBack.setBounds(1, 1, 45, getHeight());
            btnNext.setBounds(getWidth() - 45, 1, 45, getHeight());
            revalidate();
        }
        btnBack.setVisible(isControls());
        btnNext.setVisible(isControls());
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        validations.applyQualityRenderingHints(g2);

        if (getImages() != null) {
            if (getImages().length > 0 && !validations.isNullOrEmpty(imageCurrent)) {
                g2.drawImage((direction ? getImage(imagePrevious) : getImage(imageNext)), (direction ? -x : x), 0, getWidth(), getHeight(), null);
                g2.drawImage((direction ? getImage(imageCurrent) : getImage(imageCurrent)), (direction ? getWidth() - x : -getWidth() + x), 0, getWidth(), getHeight(), null);
            }
        }
    }

    private void getPositionNextBackImage(boolean next) {
        positionImage = next ? (positionImage >= images.length - 1 ? 0 : positionImage + 1) : (positionImage == 0 ? getImages().length - 1 : positionImage - 1);
        imageCurrent = getImages()[positionImage];
        imagePrevious = getImages()[positionImage == 0 ? getImages().length - 1 : positionImage - 1];
        imageNext = getImages()[positionImage >= images.length - 1 ? 0 : positionImage + 1];
        direction = next;
    }

}
