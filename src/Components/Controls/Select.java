package Components.Controls;

import Util.ComboSuggestionUI;
import javax.swing.JComboBox;

/**
 *
 * @author Codigo Patito
 * @param <E>
 */
public class Select<E> extends JComboBox<E> {

    public Select() {
        setUI(new ComboSuggestionUI());
    }

}
