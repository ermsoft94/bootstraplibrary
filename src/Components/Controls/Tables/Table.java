package Components.Controls.Tables;

import Components.Controls.Scroll.ScrollBar;
import Util.Colors;
import Util.Roboto;
import Util.Validations;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

/**
 *
 * @author Codigo Patito
 */
public class Table extends JTable {

    private final TableHeader header;
    private final TableCell cell;
    private int rollOverRowIndex = -1;

    private String colorHeader;
    private boolean hover;
    private String colorStripedOne;
    private String colorStripedTwo;
    private String colorSelectedRow;

    private final Validations validations = new Validations();

    public Table() {
        header = new TableHeader();
        cell = new TableCell();
        getTableHeader().setDefaultRenderer(header);
        getTableHeader().setPreferredSize(new Dimension(0, 40));
        setDefaultRenderer(Object.class, cell);
        setRowHeight(40);
        RollOverListener lst = new RollOverListener();
        addMouseMotionListener(lst);
        addMouseListener(lst);
    }

    public void setColumnAlignment(int column, int align) {
        header.setAlignment(column, align);
    }

    public void setCellAlignment(int column, int align) {
        cell.setAlignment(column, align);
    }

    public void setColumnWidth(int column, int width) {
        getColumnModel().getColumn(column).setPreferredWidth(width);
        getColumnModel().getColumn(column).setMinWidth(width);
        getColumnModel().getColumn(column).setMaxWidth(width);
        getColumnModel().getColumn(column).setMinWidth(10);
        getColumnModel().getColumn(column).setMaxWidth(10000);
    }

    public String getColorHeader() {
        return colorHeader;
    }

    public void setColorHeader(String colorHeader) {
        this.colorHeader = colorHeader;
    }

    public boolean isHover() {
        return hover;
    }

    public void setHover(boolean hover) {
        this.hover = hover;
    }

    public String getColorStripedOne() {
        return colorStripedOne;
    }

    public void setColorStripedOne(String colorStripedOne) {
        this.colorStripedOne = colorStripedOne;
    }

    public String getColorStripedTwo() {
        return colorStripedTwo;
    }

    public void setColorStripedTwo(String colorStripedTwo) {
        this.colorStripedTwo = colorStripedTwo;
    }

    public String getColorSelectedRow() {
        return colorSelectedRow;
    }

    public void setColorSelectedRow(String colorSelectedRow) {
        this.colorSelectedRow = colorSelectedRow;
    }

    public void fixTable(JScrollPane scroll) {
        scroll.setVerticalScrollBar(new ScrollBar(Colors.WHITE, Color.decode("#64B5F6")));
        JPanel panel = new JPanel();
        panel.setBackground(Colors.WHITE);
        scroll.setCorner(JScrollPane.UPPER_RIGHT_CORNER, panel);
        scroll.getViewport().setBackground(Colors.WHITE);
    }

    private class TableHeader extends DefaultTableCellRenderer {

        private final Map<Integer, Integer> alignment = new HashMap<>();

        public void setAlignment(int column, int align) {
            alignment.put(column, align);
        }

        @Override
        public Component getTableCellRendererComponent(JTable jtable, Object o, boolean bln, boolean bln1, int i, int i1) {
            JTableHeader header = jtable.getTableHeader();
            if (header != null) {
                setForeground(Colors.BLACK);
                setBackground(validations.isNullOrEmpty(getColorHeader()) ? Color.WHITE : Color.decode(getColorHeader()));
                setFont(Roboto.BOLD.deriveFont(14.0F));
            }
            setHorizontalAlignment(JLabel.CENTER);
            setText((o == null) ? "" : o.toString());
            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            return this;
        }
    }

    private class TableCell extends DefaultTableCellRenderer {

        private final Map<Integer, Integer> alignment = new HashMap<>();

        public void setAlignment(int column, int align) {
            alignment.put(column, align);
        }

        @Override
        public Component getTableCellRendererComponent(JTable jtable, Object o, boolean bln, boolean bln1, int row, int column) {
            Component com = super.getTableCellRendererComponent(jtable, o, bln, bln1, row, column);
            if (isCellSelected(row, column)) {
                com.setBackground(new Color(getBackground().getRed() / 255F, getBackground().getGreen() / 255F, getBackground().getBlue() / 255F, 0.6F));
            } else {
                if (row % 2 == 0) {
                    com.setBackground(validations.isNullOrEmpty(getColorStripedOne()) ? Color.decode("#dee2e6") : Color.decode(getColorStripedOne()));
                } else {
                    com.setBackground(validations.isNullOrEmpty(getColorStripedTwo()) ? Colors.WHITE : Color.decode(getColorStripedTwo()));
                }
                if (row == rollOverRowIndex && isHover()) {
                    Color col = validations.isNullOrEmpty(getColorSelectedRow()) ? Color.decode("#607D8B") : Color.decode(getColorSelectedRow());
                    com.setBackground(new Color(col.getRed() / 255F, col.getGreen() / 255F, col.getBlue() / 255F, 0.5F));
                }
            }

            com.setForeground(Colors.BLACK);
            setBorder(new EmptyBorder(0, 5, 0, 5));

            if (alignment.containsKey(column)) {
                setHorizontalAlignment(alignment.get(column));
            } else {
                setHorizontalAlignment(JLabel.LEFT);
            }
            return com;
        }

    }

    private class RollOverListener extends MouseInputAdapter {

        @Override
        public void mouseExited(MouseEvent e) {
            rollOverRowIndex = -1;
            repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            int row = rowAtPoint(e.getPoint());
            if (row != rollOverRowIndex) {
                rollOverRowIndex = row;
                repaint();
            }
        }
    }

}
