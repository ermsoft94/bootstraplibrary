package Components.Controls.Cards;

import Util.Colors;
import Util.Validations;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author Codigo Patito
 */
public class CardBlank extends JPanel {

    private boolean hover;
    private boolean mouseHover;
    private int pixels;
    private boolean dark;
    private boolean callout;
    private String colorCallout;
    private final Validations validations = new Validations();
    protected int strokeSize = 1;
    protected Dimension arcs = new Dimension(12, 12);

    public boolean isHover() {
        return hover;
    }

    public void setHover(boolean hover) {
        this.hover = hover;
        repaint();
    }

    private boolean isMouseHover() {
        return mouseHover;
    }

    private void setMouseHover(boolean mouseHover) {
        this.mouseHover = mouseHover;
        repaint();
    }

    public boolean isDark() {
        return dark;
    }

    public void setDark(boolean dark) {
        this.dark = dark;
    }

    public boolean isCallout() {
        return callout;
    }

    public void setCallout(boolean callout) {
        this.callout = callout;
    }

    public String getColorCallout() {
        return colorCallout;
    }

    public void setColorCallout(String colorCallout) {
        this.colorCallout = colorCallout;
    }

    public void setColorCallout(Color colorCallout) {
        this.colorCallout = String.format("#%06X", (0xFFFFFF & colorCallout.getRGB()));
    }

    private Color getBackgroundCard() {
        return String.format("#%06X", (0xFFFFFF & this.getBackground().getRGB())).equalsIgnoreCase("#EEEEEE") ? Colors.WHITE : this.getBackground();
    }

    public void setBackground(String color) {
        super.setBackground(Color.decode(color));
    }

    public CardBlank() {
        this.pixels = 5;
        setOpaque(false);
        setSize(new Dimension(197, 276));
        Border border = BorderFactory.createEmptyBorder(pixels, pixels, pixels, pixels);
        this.setBorder(BorderFactory.createCompoundBorder(this.getBorder(), border));
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                if (isHover()) {
                    pixels = 8;
                    setMouseHover(true);
                }
//                super.mouseEntered(e); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (isHover()) {
                    pixels = 5;
                    setMouseHover(false);
                }
//                super.mouseExited(e); //To change body of generated methods, choose Tools | Templates.
            }

        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        validations.applyQualityRenderingHints(g2);
        int shade = 0;
        int topOpacity = 80;
        int radius = (pixels % 2 == 0 ? pixels / 2 : (pixels / 2) + 1);
        for (int i = 0; i < pixels; i++) {
            g2.setColor(new Color(shade, shade, shade, ((topOpacity / pixels) * i)));
            if (isMouseHover()) {
                g2.drawLine(width - i, height - radius - 4 - i, width - i, ((radius) - i <= 0 ? radius : (pixels) - i));//linea vertical
                g2.drawLine(((radius) - i <= 0 ? radius : (pixels) - i), height - i, width - pixels - i, height - i);//linea horizontal
                g2.drawArc(width - (pixels + 6) - i, height - (pixels + 6) - i, 14, 14, -90, 90);//arco inferior derecho
            } else {
                g2.drawLine(width - 3 - i, height - radius - 8 - i, width - 3 - i, ((radius) - i <= 0 ? radius : (pixels) - i));//linea vertical
                g2.drawLine(((radius) - i <= 0 ? radius : (pixels) - i), height - 3 - i, width - 4 - pixels - i, height - 3 - i);//linea horizontal
                g2.drawArc(width - (pixels + 10) - i, height - (pixels + 10) - i, 12, 10, -95, 95);//arco inferior derecho
            }
        }
        g2.setColor(Color.decode("#607D8B"));
        g2.setStroke(new BasicStroke(strokeSize));
        g2.drawRoundRect(0, 0, width - (8), height - (9), arcs.width, arcs.height);
        int x = 1;
        int widthCallout = width - 9;
        if (isCallout()) {
            x += 8;
            widthCallout -= 9;
            g2.setColor(validations.isNullOrEmpty(getColorCallout()) ? Colors.PRIMARY : Color.decode(getColorCallout()));
            g2.fillRoundRect(1, 1, 30, height - (10), arcs.width, arcs.height);
        }
        g2.setColor(isDark() ? Colors.getColorOpacity(Color.decode("#263238"), 0.6F) : getBackgroundCard());
        g2.fillRoundRect(x, 1, widthCallout, height - 10, arcs.width, arcs.height);
    }

}
