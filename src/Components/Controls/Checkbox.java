package Components.Controls;

import Util.Colors;
import Util.Validations;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JCheckBox;

/**
 *
 * @author Codigo Patito
 */
public class Checkbox extends JCheckBox {

    private final int border = 6;
    private boolean required;
    private boolean validation;
    private final Validations validations = new Validations();

    public Checkbox() {
        setCursor(new Cursor(Cursor.HAND_CURSOR));
        setOpaque(false);
        setBackground(new Color(69, 124, 235));
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isValidation() {
        validation = true;
        repaint();
        return validation;
    }

    @Override
    public void paint(Graphics grphcs) {
        super.paint(grphcs);
        Graphics2D g2 = (Graphics2D) grphcs;
        validations.applyQualityRenderingHints(g2);
        int ly = (getHeight() - 16) / 2;
        if (isSelected()) {
            validation = false;
            if (isEnabled()) {
                g2.setColor(getBackground());
            } else {
                g2.setColor(Color.GRAY);
            }
            g2.fillRoundRect(1, ly, 16, 16, border, border);
            int px[] = {4, 8, 14, 12, 8, 6};
            int py[] = {ly + 8, ly + 14, ly + 5, ly + 3, ly + 10, ly + 6};
            g2.setColor(Color.WHITE);
            g2.fillPolygon(px, py, px.length);
        } else {
            if (isRequired() && !isSelected() && validation) {
                g2.setColor(isSelected() ? Colors.SUCCESS : Colors.DANGER);
                g2.fillRoundRect(1, (getHeight() - 16) / 2, 16, 16, border, border);
            } else {
                g2.setColor(Color.GRAY);
                g2.fillRoundRect(1, ly, 16, 16, border, border);
            }
            g2.setColor(Color.WHITE);
            g2.fillRoundRect(2, ly + 1, 14, 14, border, border);

        }
        g2.dispose();
    }

}
