package Util;

/**
 *
 * @author Codigo Patito
 */
public interface EventSwitchSelected {

    public void onSelected(boolean selected);

}
