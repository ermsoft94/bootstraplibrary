package Components.Alerts.SweetAlert;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.WindowConstants;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.TimingTarget;
import org.jdesktop.animation.timing.TimingTargetAdapter;

/**
 *
 * @author Codigo Patito
 */
public class Swal extends JDialog {

    private Background content;
    private Animator animator;
    private Dimension originalSize;
    private Point originalLocation;
    private boolean show;
    private boolean result;

    public Swal(Frame fram, boolean modal) {
        super(fram, modal);
        init();
    }

    private void init() {
        setUndecorated(true);
        setBackground(new Color(0, 0, 0, 0));
        content = new Background();
        content.setBackground(new Color(0, 0, 0, 0.2F));
        setContentPane(content);
        TimingTarget target = new TimingTargetAdapter() {
            @Override
            public void end() {
                if (show == false) {
                    dispose();
                }
            }

            @Override
            public void timingEvent(float fraction) {
                float f;
                if (show) {
                    f = fraction;
                } else {
                    f = 1f - fraction;
                }
                int w = (int) (originalSize.width * f);
                int h = (int) (originalSize.height * f);
                int x = originalLocation.x - w / 2;
                int y = originalLocation.y - h / 2;
                setLocation(x, y);
                setSize(new Dimension(w, h));
            }
        };
        animator = new Animator(800, target);
        animator.setInterpolator((float f) -> {
            if (show) {
                return easeOutBack(f);
            } else {
                return easeOutExpo(f);
            }
        });
        animator.setResolution(0);
    }

    private float easeOutBack(float x) {
        float c1 = 1.70158F;
        float c3 = c1 + 1;
        return (float) (1 + c3 * Math.pow(x - 1, 3) + c1 * Math.pow(x - 1, 2));
    }

    private float easeOutExpo(float x) {
        return (float) (x == 1 ? 1 : 1 - Math.pow(2, -10 * x));
    }

    public boolean showAlert(int duration) {
        showAlertEvent(duration);
        return result;
    }

    public boolean showAlert() {
        showAlertEvent(800);
        return result;
    }

    private void showAlertEvent(int duration) {
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        originalSize = getPreferredSize();
        originalLocation = getLocation(getParent());
        setSize(new Dimension(0, 0));
        if (animator.isRunning()) {
            animator.stop();
        }
        show = true;
        animator.setDuration(duration);
        animator.start();
        setVisible(true);
    }

    public void closeAlert(boolean result) {
        this.result = result;
        closeAlertEvent(800);
    }

    public void closeAlert(int duration) {
        closeAlertEvent(duration);
    }

    public void closeAlert(boolean result, int duration) {
        this.result = result;
        closeAlertEvent(duration);
    }

    public void closeAlert() {
        closeAlertEvent(800);
    }

    private void closeAlertEvent(int duration) {
        if (animator.isRunning()) {
            animator.stop();
        }
        show = false;
        animator.setDuration(duration);
        animator.start();
    }

    public Point getLocation(Container parent) {
        Point location = parent.getLocationOnScreen();
        Dimension size = parent.getSize();
        int x = location.x + size.width / 2;
        int y = location.y + size.height / 2;
        Point point = new Point(x, y);
        return point;
    }

    private class Background extends JComponent {

        @Override
        public void paint(Graphics grphcs) {
            Graphics2D g2 = (Graphics2D) grphcs.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setColor(getBackground());
            g2.fillRoundRect(0, 0, getWidth(), getHeight(), 17, 17);
            g2.dispose();
            super.paint(grphcs);
        }
    }

}
