package Util;

import javax.swing.JTextArea;

/**
 *
 * @author Codigo Patito
 */
public class AttributesTextArea extends JTextArea {

    private String placeHolder;
    public boolean validate;
    private boolean validation;
    private int maxlength;
    private int minlength;
    private String pattern;
    private boolean required;
    private String invalidFeedback;
    private String validFeedback;

    public String getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(String placeHolder) {
        this.placeHolder = placeHolder;
    }

    public int getMaxlength() {
        return maxlength;
    }

    public void setMaxlength(int maxlength) {
        this.maxlength = maxlength;
    }

    public int getMinlength() {
        return minlength;
    }

    public void setMinlength(int minlength) {
        this.minlength = minlength;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public boolean isValidation() {
        return validation;
    }

    public boolean validateField() {
        validate = true;
        validation = validation();
        this.repaint();
        return validation();
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getInvalidFeedback() {
        return invalidFeedback;
    }

    public void setInvalidFeedback(String invalidFeedback) {
        this.invalidFeedback = invalidFeedback;
    }

    public String getValidFeedback() {
        return validFeedback;
    }

    public void setValidFeedback(String validFeedback) {
        this.validFeedback = validFeedback;
    }

    private boolean validation() {
        return isEmptyValidation();
    }

    private boolean isEmptyValidation() {
        if (getMaxlength() > 0 && getMinlength() > 0) {
            return getText().length() <= getMaxlength() && getText().length() >= getMinlength();
        }
        if (getMaxlength() > 0 && getMinlength() == 0) {
            return getText().length() <= getMaxlength() && getText().length() > 0;
        }
        if (getMaxlength() == 0 && getMinlength() > 0) {
            return getText().length() >= getMinlength();
        }
        if (isRequired()) {
            return !getText().isEmpty();
        }
        return true;
    }

    private String getEmptyValidationDescription() {
        if (getMaxlength() > 0 && getMinlength() > 0) {
            if (getText().length() >= getMaxlength() && getText().length() <= getMinlength()) {
                return "La longitud debe ser " + getText().length() + " <= " + getMaxlength() + " y " + getText().length() + " >= " + getMinlength();
            }
        }
        if (getMaxlength() > 0 && getMinlength() == 0) {
            if (getText().length() >= getMaxlength()) {
                return "La longitud debe ser " + getText().length() + " <= " + getMaxlength();
            }
        }
        if (getMaxlength() == 0 && getMinlength() > 0) {
            if (getText().length() <= getMinlength()) {
                return "La longitud debe ser " + getText().length() + " >= " + getMinlength();
            }
        }
        if (isRequired()) {
            if (getText().isEmpty()) {
                return "El campo es requerido";
            }
        }
        return "";
    }

    public String getErrors() {
        return getEmptyValidationDescription();
    }
}
