package Util;

import java.awt.Color;

/**
 *
 * @author Codigo Patito
 */
public enum TypeButton {

    PRIMARY, SECONDARY, SUCCESS, DANGER, WARNING, INFO, LIGHT, DARK;

    public static Color getColor(TypeButton type) {
        switch (type) {
            case PRIMARY:
                return Colors.PRIMARY;
            case SECONDARY:
                return Colors.SECONDARY;
            case SUCCESS:
                return Colors.SUCCESS;
            case DANGER:
                return Colors.DANGER;
            case WARNING:
                return Colors.WARNING;
            case INFO:
                return Colors.INFO;
            case LIGHT:
                return Colors.LIGHT;
            case DARK:
                return Colors.DARK;
        }
        return Colors.PRIMARY;
    }

    public static Color getColorHover(TypeButton type) {
        switch (type) {
            case PRIMARY:
                return Colors.PRIMARY_HOVER;
            case SECONDARY:
                return Colors.SECONDARY_HOVER;
            case SUCCESS:
                return Colors.SUCCESS_HOVER;
            case DANGER:
                return Colors.DANGER_HOVER;
            case WARNING:
                return Colors.WARNING_HOVER;
            case INFO:
                return Colors.INFO_HOVER;
            case LIGHT:
                return Colors.LIGHT_HOVER;
            case DARK:
                return Colors.DARK_HOVER;
        }
        return Colors.PRIMARY_HOVER;
    }

    public static Color getTextColor(TypeButton type) {
        switch (type) {
            case PRIMARY:
                return Colors.WHITE;
            case SECONDARY:
                return Colors.WHITE;
            case SUCCESS:
                return Colors.WHITE;
            case DANGER:
                return Colors.WHITE;
            case WARNING:
                return Colors.BLACK;
            case INFO:
                return Colors.BLACK;
            case LIGHT:
                return Colors.BLACK;
            case DARK:
                return Colors.WHITE;
        }
        return Colors.WHITE;
    }

}
