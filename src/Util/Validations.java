package Util;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Codigo Patito
 */
public class Validations {

    private final String regexEmail = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
    private final String regexOnlyLetters = "^[\\pL\\s]*$";//"^[a-zA-Z\\s]+$";
    private final String regexOnlyIntegers = "^\\d+$";
    private final String regexNumbers = "^(\\d)+([.]\\d+)?$";

    public Validations() {
    }

    public boolean patternMatches(String text, String regex) {
        return Pattern.compile(regex).matcher(text).matches();
    }

    public String getRegexEmail() {
        return regexEmail;
    }

    public String getRegexOnlyLetters() {
        return regexOnlyLetters;
    }

    public String getRegexOnlyIntegers() {
        return regexOnlyIntegers;
    }

    public String getRegexNumbers() {
        return regexNumbers;
    }

    public boolean isValidEmail(String value) {
        return patternMatches(value, getRegexEmail());
    }

    public boolean isValidOnlyLetters(String value) {
        return patternMatches(value, getRegexOnlyLetters());
    }

    public boolean isValidOnlyIntegers(String value) {
        return patternMatches(value, getRegexOnlyIntegers());
    }

    public boolean isValidNumbers(String value) {
        return patternMatches(value, getRegexNumbers());
    }

    public boolean isNullOrEmpty(Object object) {
//        return value == null || value.isEmpty();
        return (null == object || (object instanceof String && object.toString().isEmpty()));
    }

    public Icon resizeIcon(ImageIcon icon, int resizedWidth, int resizedHeight) {
        Image img = icon.getImage();
        Image resizedImage = img.getScaledInstance(resizedWidth, resizedHeight, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }

    public Icon resizeIcon(String path, int resizedWidth, int resizedHeight) {
        Image img = new javax.swing.ImageIcon(getClass().getResource(path)).getImage();
        Image resizedImage = img.getScaledInstance(resizedWidth, resizedHeight, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }

    public void applyQualityRenderingHints(Graphics2D g2d) {
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
    }

}
