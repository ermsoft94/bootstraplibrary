package Util;

import javax.swing.JTextField;

/**
 *
 * @author Codigo Patito
 */
public class Attributes extends JTextField {

    private String placeHolder;
    public boolean validate;
    private boolean validation;
    private Type type = Type.TEXT;
    private int maxlength;
    private int minlength;
    private String pattern;
    private boolean required;
    private final Validations validations = new Validations();
    private String invalidFeedback;
    private String validFeedback;

    public String getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(String placeHolder) {
        this.placeHolder = placeHolder;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getMaxlength() {
        return maxlength;
    }

    public void setMaxlength(int maxlength) {
        this.maxlength = maxlength;
    }

    public int getMinlength() {
        return minlength;
    }

    public void setMinlength(int minlength) {
        this.minlength = minlength;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public boolean isValidation() {
        return validation;
    }

    public boolean validateField() {
        validate = true;
        validation = validation();
        this.repaint();
        return validation();
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getInvalidFeedback() {
        return invalidFeedback;
    }

    public void setInvalidFeedback(String invalidFeedback) {
        this.invalidFeedback = invalidFeedback;
    }

    public String getValidFeedback() {
        return validFeedback;
    }

    public void setValidFeedback(String validFeedback) {
        this.validFeedback = validFeedback;
    }

    private boolean validation() {
        if (isEmptyValidation()) {
            return typeValidation();
        }
        return false;
    }

    private boolean isEmptyValidation() {
        if (getMaxlength() > 0 && getMinlength() > 0) {
            return getText().length() <= getMaxlength() && getText().length() >= getMinlength();
        }
        if (getMaxlength() > 0 && getMinlength() == 0) {
            return getText().length() <= getMaxlength() && getText().length() > 0;
        }
        if (getMaxlength() == 0 && getMinlength() > 0) {
            return getText().length() >= getMinlength();
        }
        if (isRequired()) {
            return !getText().isEmpty();
        }
        return true;
    }

    private String getEmptyValidationDescription() {
        if (getMaxlength() > 0 && getMinlength() > 0) {
            if (getText().length() >= getMaxlength() && getText().length() <= getMinlength()) {
                return "La longitud debe ser " + getText().length() + " <= " + getMaxlength() + " y " + getText().length() + " >= " + getMinlength();
            }
        }
        if (getMaxlength() > 0 && getMinlength() == 0) {
            if (getText().length() >= getMaxlength()) {
                return "La longitud debe ser " + getText().length() + " <= " + getMaxlength();
            }
        }
        if (getMaxlength() == 0 && getMinlength() > 0) {
            if (getText().length() <= getMinlength()) {
                return "La longitud debe ser " + getText().length() + " >= " + getMinlength();
            }
        }
        if (isRequired()) {
            if (getText().isEmpty()) {
                return "El campo es requerido";
            }
        }
        return "";
    }

    private boolean typeValidation() {
        boolean bnd = false;
        switch (type) {
            case NUMBER:
                bnd = validations.isValidNumbers(getText());
                break;
            case NUMBER_INTEGERS:
                bnd = validations.isValidOnlyIntegers(getText());
                break;
            case EMAIL:
                bnd = validations.isValidEmail(getText());
                break;
            case TEXT_LETTERS:
                bnd = validations.isValidOnlyLetters(getText());
                break;
            case TEXT:
                bnd = true;
                if (getPattern() != null) {
                    bnd = validations.patternMatches(getText(), getPattern());
                }
                break;
        }
        bnd = isRequired() ? bnd : (validations.isNullOrEmpty(this.getText()) ? true : bnd);
        return bnd;
    }

    private String getTypeValidationDescription() {
        String error = "";
        switch (type) {
            case NUMBER:
                if (!validations.isValidNumbers(getText())) {
                    error = "Solo numeros";
                }
                break;
            case NUMBER_INTEGERS:
                if (!validations.isValidOnlyIntegers(getText())) {
                    error = "Solo numeros enteros";
                }
                break;
            case EMAIL:
                if (!validations.isValidEmail(getText())) {
                    error = "El correo electronico no es valido";
                }
                break;
            case TEXT_LETTERS:
                if (!validations.isValidOnlyLetters(getText())) {
                    error = "Solo letras";
                }
                break;
            case TEXT:
                if (getPattern() != null) {
                    if (!validations.patternMatches(getText(), getPattern())) {
                        error = "no coinciden con el patron";
                    }
                }
                break;
        }
        return error;
    }

    public String getErrors() {
        return (getEmptyValidationDescription() + " " + getTypeValidationDescription()).trim();
    }
}
