package Components.Controls;

import Util.Colors;
import Util.Validations;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JRadioButton;

/**
 *
 * @author Codigo Patito
 */
public class Radio extends JRadioButton {

    private boolean required;
    private boolean validation;
    private final Validations validations = new Validations();

    public Radio() {
        setOpaque(false);
        setCursor(new Cursor(Cursor.HAND_CURSOR));
        setBackground(new Color(69, 124, 235));
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isValidation() {
        validation = true;
        repaint();
        return validation;
    }

    @Override
    public void paint(Graphics grphcs) {
        super.paint(grphcs);
        Graphics2D g2 = (Graphics2D) grphcs;
        validations.applyQualityRenderingHints(g2);
        int ly = (getHeight() - 16) / 2;
        if (isSelected()) {
            validation = false;
            if (isEnabled()) {
                g2.setColor(getBackground());
            } else {
                g2.setColor(Color.GRAY);
            }
            g2.fillOval(1, ly, 16, 16);

            g2.fillOval(2, ly + 1, 14, 14);

            g2.setColor(Color.WHITE);
            g2.fillOval(5, ly + 4, 8, 8);
        } else {
            if (isRequired() && !isSelected() && validation) {
                g2.setColor(isSelected() ? Colors.SUCCESS : Colors.DANGER);
            } else {
                g2.setColor(getBackground());
            }
            g2.fillOval(1, ly, 16, 16);
            g2.setColor(Color.WHITE);
            g2.fillOval(2, ly + 1, 14, 14);
        }
    }

}
