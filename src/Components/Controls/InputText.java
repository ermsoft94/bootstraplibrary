package Components.Controls;

import Util.Attributes;
import Util.Colors;
import Util.RoundedBorder;
import Util.Validations;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 *
 * @author Codigo Patito
 */
public class InputText extends Attributes {

    private final int radius = 12;
    private final Validations validations = new Validations();

    public InputText() {
        setOpaque(false);
        setFont(new Font("Tahoma", 0, 14));
        setBorder(new RoundedBorder(5, Colors.TRANSPARENT));
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Colors.WHITE);
        if (!isEnabled() || !isEditable()) {
            g2.setColor(new Color(getBackground().getRed() / 255F, getBackground().getGreen() / 255F, getBackground().getBlue() / 255F, 0.5F));
        }
        g2.fillRoundRect(1, 1, getWidth() - 1, getHeight() - 1, radius, radius);

        if (validations.isNullOrEmpty(getText()) && !validations.isNullOrEmpty(getPlaceHolder())) {
            g2.setColor(getDisabledTextColor());
            g2.setFont(getFont());
            g2.drawString(getPlaceHolder(), getInsets().left, (getHeight() / 2) + 5);
        }
        super.paintComponent(g2);
    }

    @Override
    protected void paintBorder(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        validations.applyQualityRenderingHints(g2);
        if (validate) {
            g2.setStroke(new BasicStroke(2));
            boolean val = isValidation();
            g2.setColor(Colors.getColorOpacity(val ? Colors.SUCCESS : Colors.DANGER, 0.6F));
            if (!validations.isNullOrEmpty(getValidFeedback()) || !validations.isNullOrEmpty(getInvalidFeedback())) {
                g2.setFont(new Font("Tahoma", 0, 12));
                if (val && !validations.isNullOrEmpty(getValidFeedback())) {
                    g2.drawString(getValidFeedback(), 3, getHeight() - 4);
                }
                if (!val && !validations.isNullOrEmpty(getInvalidFeedback())) {
                    g2.drawString(getInvalidFeedback(), 3, getHeight() - 4);
                }
            }
        } else {
            if (isFocusOwner() && isEnabled() && isEditable()) {
                g2.setColor(Colors.getColorOpacity(Color.decode("#86b7fe"), 0.5F));
                g2.setStroke(new BasicStroke(2));
            } else {
                g2.setColor(Colors.getColorOpacity(Colors.SECONDARY, 0.7F));
                g2.setStroke(new BasicStroke(1));
            }
        }
        g2.drawRoundRect(0, 0, getWidth(), getHeight(), radius, radius);
    }

}
