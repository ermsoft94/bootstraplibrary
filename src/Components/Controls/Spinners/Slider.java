package Components.Controls.Spinners;

import Util.JSliderUI;
import java.awt.Color;
import javax.swing.JSlider;

/**
 *
 * @author Codigo Patito
 */
public class Slider extends JSlider {

    public Slider() {
        setOpaque(false);
        setBackground(new Color(180, 180, 180));
        setForeground(new Color(69, 124, 235));
        setUI(new JSliderUI(this));
    }

}
