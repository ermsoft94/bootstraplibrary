package Util;

import java.awt.Font;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Codigo Patito
 */
public class Roboto {

    public static final Font BLACK = loadFont("Roboto-Black.ttf").deriveFont(1);

    public static final Font BLACK_ITALIC = loadFont("Roboto-BlackItalic.ttf").deriveFont(3);

    public static final Font BOLD = loadFont("Roboto-Bold.ttf").deriveFont(1);

    public static final Font BOLD_ITALIC = loadFont("Roboto-BoldItalic.ttf").deriveFont(3);

    public static final Font ITALIC = loadFont("Roboto-Italic.ttf").deriveFont(2);

    public static final Font LIGHT = loadFont("Roboto-Light.ttf").deriveFont(0);

    public static final Font LIGHT_ITALIC = loadFont("Roboto-LightItalic.ttf").deriveFont(2);

    public static final Font MEDIUM = loadFont("Roboto-Medium.ttf").deriveFont(0);

    public static final Font MEDIUM_ITALIC = loadFont("Roboto-MediumItalic.ttf").deriveFont(2);

    public static final Font REGULAR = loadFont("Roboto-Regular.ttf").deriveFont(0);

    public static final Font THIN = loadFont("Roboto-Thin.ttf").deriveFont(0);

    public static final Font THIN_ITALIC = loadFont("Roboto-ThinItalic.ttf").deriveFont(2);

    private static Font loadFont(String resourceName) {
        try (InputStream inputStream = Roboto.class.getResourceAsStream("/fonts/" + resourceName)) {
            return Font.createFont(0, inputStream);
        } catch (IOException | java.awt.FontFormatException e) {
            throw new RuntimeException("Could not load " + resourceName, e);
        }
    }

}
